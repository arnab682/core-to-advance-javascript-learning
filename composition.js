// two type composition function composition and object composition
//function composition
function print(inp) {
  console.log(inp);
}

function multiplyByFive(n) {
  return n * 5;
}

function add(a, b) {
  return a + b;
}

print(multiplyByFive(add(5,2)));