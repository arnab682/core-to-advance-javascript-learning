function greet(msg){
  function greeting(name){
    return msg + ', ' + name;
  }
  return greeting;
}

var gm = greet('Good Morning');
var gn = greet('Good Night');
var h = greet('Hello');

console.log(gm('Arnab Das'));
console.log(gn('Arnab Das'));
console.log(h('Arnab Das'));


// another example
function base(x){
  var result = 1;
 
  return function(n){ //Anonymas function
    for(var i = 0; i < n; i++){
      result *= x;
    }
    return result;
  };
}
var base10 = base(10);
var base8 = base(8);
var base5 = base(5);
console.log(base10(2));
console.log(base8(3));
console.log(base5(2));