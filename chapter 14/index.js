//prototype inheritance

function Shape(name){
  this.name = name;
}

//good practice Shape.prototype.common
// Shape.prototype.common = function() {
//   console.log('I am Common Method');
// }
Shape.prototype = { //this system have a get problem because it(Shape.prototype) is remove old prototype data and store new prototype data
  common: function() {
    console.log('I am Common Method');
  }
}
var sp = new Shape(); 
console.log(sp);

function Square(width, name){
  Shape.call(this, name);
  this.width = width;
}
var sq = new Square(); 
console.log(sq);

//this is double
//Square.prototype = Object.create(Shape.prototype);//inherit, prototype reset //only share shape prototype not share this insant property
//Square.prototype.constructor = Square; //constructor reset

extend(Shape, Square);

//create a function for prototype reset
function extend(Parent, Child) {
  Child.prototype = Object.create(Parent.prototype);//inherit, prototype reset //only share shape prototype not share this insant property
  Child.prototype.constructor = Child; //constructor reset
}


Square.prototype.draw = function() {
  console.log('Drawing');
}
var shape = new Shape('Arnab');
console.log(shape);
var sqr = new Square(45, 'Anik');
console.log(sqr); // common from shape prototype inherit

//shape->Shape->Object
//sqr->Square->Object

//sqr->Square->Shape->Object

function Circle(radius, name) {
    Shape.call(this, name);
    this.radius = radius;
}
//circle.prototype = Object.create(Shape);
//circle.prototype = Object.create(Shape.prototype);
//circle.prototype.constructor = circle;
extend(Shape, Circle);

var circle = new Circle(22, 'Green');
console.log(circle);
console.log(Object.keys(circle));
console.log(circle.common());


//another way for create class from function
function Shape1(name){
  this.name = name;
}

extend(Shape, Shape1);


//It is bad practice
//var shape1 = new Shape1.prototype.constructor('Anik');
var shape1 = new Shape1('Anik');
console.log(shape1);


//If needed for update common function 
// shape1.prototype.common = function() {
//   Shape.prototype.common.call(this);
//   console.log('It is common function from Shape1 and I am override');
// }
console.log('lol');
console.log(shape1.common());


//If needed for update common function 
Circle.prototype.common = function() {
  Shape.prototype.common.call(this);
  console.log('It is common function from Circle and I am override');
}
console.log(circle.common());


var shapes = [sqr, shape1, circle];

//Polymorphism
//one object different property display
for (var i of shapes){
  console.log(i.common());
}

//check to object
console.log(sp instanceof Shape);


//Inheritance maltiplu class
var canWalk = {
  walk: function() {
    console.log('Walking...');
  }
}

var canEat = {
  cat: function() {
    console.log('Eating...');
  }
}
//ES6 apply
//two function including in one class
//var person = Object.assign({}, canWalk, canEat);
//console.log(person);
//person.name = 'Arnab';

function Person(name) {
  this.name = name;
}


//Object.assign(Person.prototype, canWalk, canEat);
//or
function mixin(target, ...source) {
    Object.assign(target, ...source);
} 
mixin(Person.prototype, canEat,canWalk);

var person = new Person('Arnab Das Anik');
console.log(person);

var canSwin = {
  swim: function() {
    console.log('Swimming...');
  }
}

function GoldFish(name) {
  this.name = name;
}

mixin(GoldFish.prototype, canSwin, canEat);

var fish = new GoldFish('bla bla');
console.log(fish);