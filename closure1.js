// Closure is when a functin is able to remember and access it's lexial scope even when that function executing outside it's lexical scope

//able to remember and access it's lexial scope
//when that function executing outside it's lexical scope

// function test() {
//   var msg = 'I am learning Lexical Scope and Closure';
//   function sayMsg() {
//     console.log(msg);
//   }
//   sayMsg();
// }
// test();

// function test() {
//   var msg = 'I am learning Lexical Scope and Closure';
//   return function () { //another function
//     console.log(msg);
//   }
// }
// test(); // it is not access in return function
// var anonymasFuc = test();
// anonymasFuc(); 


// for (var i = 1; i <= 5; i++){
//   setTimeout(function() {
//     console.log(i); // i=6 have a problem for setTime
//   }, 1000 * i); //output 6 6 6 6 6
// }

//solve using js immediate invoke function
for (var i = 1; i <= 5; i++){
    (function (n) {
      setTimeout(function() {
        console.log(n); // i=6 have a problem for setTime
      }, 1000 * n); //output 1 2 3 4 5 
    })(i)
}