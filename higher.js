function add(a, b) {
  return a+b;
}

function manipulate(a, b, func) {
  var c = a + b;
  var d = a - b;
  // function multiply() {
  //   var m = func(a, b);
  //   return a*b*m;
  // }
  // return multiply;

  return function() {
    var m = func(a, b);
    return a*b*m;
  }
}
var multiply = manipulate(2, 5, add);
console.log(multiply);
console.log(multiply());