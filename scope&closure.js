function a(){
  b();
  console.log('a function');
}
function b(){
  d();
  console.log('b function');
}
function c(){
  console.log('c function');
}
function d(){
  c();
  console.log('d function');
}

var x = 100;
a();
console.log('I am Global ' + x);


//creational phase
//executional phase


//variable 
var aa = 10;

var bb;

console.log(aa);
console.log(bb);

bb = 20;
console.log(bb);

console.log(cc);

var cc = 50;
console.log(cc);

//creational Phase
//aa = undefined
//bb = undefined
//cc = undefined

//Executional Phase
// aa = 10 //console.log
// bb = undefined
// bb = 20
// cc = undefined
// cc = 50


abc();
function abc(){
  console.log('Iam Function');
}
abc();

//creational phase 
//abc = refernce

//Executional Phase


//Hoisting
var v = 100;
//newPrint(v); //undefind [Error]

print(10);

var newPrint = print;
newPrint(45);

function print(v) {
  console.log(v);
}

print(v);

//creational phase
//v = undefind
//newPrint = undefind
//print = refernce

//Executional phase
//v = 100
// undefind [Error]
// 10
// newPrint = ref(print)
// 45
// 100



// Function Declaration vs Expression

//declaration
abcd();
function abcd(){
  console.log('I am Function');
}
abcd();
//creational phase
//abcd = refernce

//Executional phase
//abcd = refernce
//abcd = refernce


//Expression
//newAbcd();
var newAbcd = function () {
  console.log('I am new abcd expression function');
}
newAbcd();
//creational phase
//newAbcd = undefind

//executional phase
//undefind
//newAbcd = refernce


//scrope
var t = 10;
function tt(){
  var t = 20;
  console.log(t);
}
tt();
console.log(t);

//Nested Scope
var x = 55
function test(){
  var x = 45;
  console.log(x);
  function nested() {
    var y = 65;
    console.log(x);
  }
  nested();
}
test();
console.log(x);


var aa = 11;
function AA() {
  var bb = 12
  function BB() {
    var cc = 23;
    console.log('cc ->' + cc);
  }

  function CC() {
    var dd = 56;
    console.log('dd ->' + dd);
  }
  console.log('bb ->' + bb)
  DD(bb);
  BB();
  CC();
}

function DD(n) {
  return n + aa;
}

// A -> a, b, B(), C(), D()
// B -> a, b, c, B(), C(), D()
// C -> a, b, d, B(), C(), D()
// D -> a, n, A()