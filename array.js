var arr = [1, 2, 3, 4]; // square bracket //basic type of array
console.log(arr); 
console.log(arr[1]); 
arr[4] = 5;
console.log(arr); 
arr[10] = 11; //replace
console.log(arr); 
console.log(arr[6]); //undefind array

console.log(arr.length); //11

//replace
var arr = [2, 4, 6, 8];
console.log(arr);
arr[2] = 2;
console.log(arr);

var arr2 = Array();
console.log(arr2);
var arr2 = Array(1,2,'ADA',4);
console.log(arr2);


var arr = ['A', 'r', 'n', 'a', 'b'];
console.log(arr.length);
for (var i = 0; i < arr.length; i++) {
  console.log(arr[i]); // Array Travers
}

var results = Array();
for (var i = 0; i < arr.length; i++) {
  results[i] = arr[i];
  //console.log(i);
}
console.log(results);



var arr = [1,2,3,4,5,6];
console.log(arr.length);
for (var i = 0; i < arr.length; i++) {
  //console.log(arr[i]); // Array Travers
  if(arr[i]%2 === 1){ //onlt odd number will be change
    arr[i] = arr[i] + 1;
  } 
}
console.log(arr);

var arrSum = [2,4,6,8];
var sum = 0;
for (var i = 0; i < arrSum.length; i++) {
  //sum = sum + arrSum[i];
  sum += arrSum[i];
}
console.log(sum);



// Insert and Remove Element
var arr = [1,2,3,4,5,6,7,8];
// Insert 9 in 3 number index
//arr[3] = 9; //It is replace
//arr.push(9); //It is push in array's last position
//arr.unshift(9); //It added to array first position

arr.splice(3, 0, 9, 10); // splice(index, how many remove, and new data insert)
console.log(arr);

arr.splice(3, 2, 42);
console.log(arr);


var arr = [1,2,3,4,5,6];
//arr[3] = undefined; //It is not remove this index;
//arr[3] = null; //It is not remove this index;
//arr.pop(); //It is means last index remove
//arr.shift();// It is means first index remove

//arr.splice(2, 2);//remove
arr.splice(2, 2, 20); // remove and add

console.log(arr);


//Search a array
var arr = [1,3,5,7,9]; //Liniar Array
var find = 5;
var isFound = false;

for (var i = 0; i < arr.length; i++) {
  if(arr[i] === find) {
    console.log('Data found in ' + i);
    isFound = true;
    break;
  }
}
if (!isFound) {
  console.log('Data is not found');
}


// Multidimentional Array
var arr = [
    
    [2,3,4,5],
    [6,7,8,9],
    [0,1,4,2],
    [0,9,6,8]
]
console.log(arr);

for (var i = 0; i < arr.length; i++) {
  var results = '';
  for(var j = 0; j < arr[i].length; j++){
    // console.log(arr[i][j]);
    results += arr[i][j] + ' ';
  }
  console.log('Element of : ' + results);
}


// Array Reverse

var arr = [1, 3, 4, 6, 7, 8];

for(var i = 0; i < (arr.length / 2); i++) {
  var temp = arr[i]; // first data store in a variable
  arr[i] = arr[arr.length - (i+1)]; // last data store in first index
  arr[arr.length - (i+1)] = temp; // and 1st data variable store in last index
  // break;

}
console.log(arr);

var arr1 = [3, 5, 7, 9];
console.log(arr1.reverse()); //reverse method call


//Array Method
var arr = [2, 5, 7, 0];
console.log(arr);
console.log(arr.join(' ')); 
console.log(arr.join(', '));

console.log(arr.fill(0));
console.log(arr.fill(true));

var arr = [1, 2, 3, 4, 5, 6]
var arr2 = [2, 4, 5, 7];

console.log(arr.concat(arr2));

console.log(Array.isArray(arr));


var a = [1, 3];
var b = a;
console.log(b);
console.log(a);
b[0] = 2;
console.log(b);
console.log(a);

//defferent
var a = [1, 3];
var b = Array.from(a);
console.log(b);
console.log(a);
b[0] = 2;
console.log(b);
console.log(a);