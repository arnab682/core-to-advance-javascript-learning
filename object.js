// Object Literal
var obj = {}; // Object datatype
console.log(obj);
console.log(typeof obj);

obj.x = 5;
console.log(obj);

//Object Literal
var point = {
  x: 10,
  y: 20 
};

point.z = 30;
console.log(point);


//Object Constructor
var obj = Object();
obj.a = 5;
console.log(obj);

var obj2 = new Object();
obj2.b = 20;
obj2.c = 40;
console.log(obj2.b + obj2.c); // dot notation
console.log(obj2['b'] + obj2['c']); // array notation
console.log(obj2.d); //undefind

obj2.b = 25;
var prop = 'c';
obj2[prop] = 22;
console.log(obj2);

delete obj2.b; //remove properties
console.log(obj2);

//comparing two object
var obj1 = {
  a: 10,
  b: 20
}

var obj2 = {
  a: 10,
  b: 20
}

console.log(obj1 === obj2); //false
console.log(obj1.a === obj2.a && obj1.b === obj2.b); //true

console.log(obj1); // real value
console.log(JSON.stringify(obj1)); // object to string convert 
console.log(obj2); // real value
console.log(JSON.stringify(obj2)); // object to string convert 
console.log(JSON.stringify(obj1) === JSON.stringify(obj2)); //true



// Iterate Object Properties
var obj3 = {
  x: 40,
  y: 60,
  z: 75
}
console.log('x' in obj3); //true
console.log('q' in obj3); //false

for (var i in obj3) { // finding how many properties in obj3 Object 
  console.log(i); // properties name
  console.log(i + ': ' + obj3[i]); // properties value
}

console.log(Object.keys(obj3)); //kes array convert
console.log(Object.values(obj3)); //values array convert
console.log(Object.entries(obj3)); //two dymentional array convert


//have a problem solve
// var obj4 = obj3;
// obj4.x = 2; // it is change in obj3, obj4

// console.log(obj3);
// console.log(obj4);

//solve
var obj4 = Object.assign({}, obj3); // obj4 = obj3
obj4.x = 100; // it is not change obj3 

console.log(obj3);
console.log(obj4);
