var arr = [1, 4, 2, 3, 7, 9, 0];

var find = arr.find(function(value, index) {
    return value === 7;
});
console.log(find);

function myFind(arr, cb) {
  for (var i = 0; i < arr.length; i++) {
    if (cb(arr[i])) {
      //return arr[i];
      return i;
    }
  }
}
var result = myFind(arr, function(value) {
  return value === 9;
});
console.log(result);