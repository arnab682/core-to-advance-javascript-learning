var arr = [2, 4, 5, -10, 8, 2, 3, 1, 0, -2, -55];
 //arr.sort(); // when have negative value it is not working
 console.log(arr);

var persions = [
    {
      name: 'B',
      age: 12
    },
    {
      name: 'D',
      age: 11
    },
    {
      name: 'C',
      age: 13
    },
    {
      name: 'A',
      age: 7
    },
  ];

  persions.sort(); //no changing no sorting 
  console.log(persions);

  //sorting
  arr.sort(function(a, b) {
    if(a>b) {
      return 1;
    }
    else if (a<b) {
      return -1;
    }
    else{
      return 0;
    }
  });
  console.log(arr); //ascending

  arr.sort(function(a, b) {
    if(a>b) {
      return -1;
    }
    else if (a<b) {
      return 1;
    }
    else{
      return 0;
    }
  });
  console.log(arr); //decending

  //persions object sorting
  persions.sort(function(a, b) {
    if(a.age > b.age) {
      return 1;
    }
    else if (a.age < b.age) {
      return -1;
    }
    else{
      return 0;
    }
  });
  console.log(persions);

var arr = [2, 3, 5, 1, 5, 78, 98, 0];
 //filter even number only
var res = arr.every(function(value) {
  return value % 2 == 0;
});
console.log(res); //false

var arr = [2, 3, 5, 1, 5, 78, 98, 0];
 //filter even number only
var res = arr.every(function(value) { //every function given true false
  return value >= 0;
});
console.log(res); //true

//some function check for nagetive number it is given true false
var arr = [2, 3, 5, 1, 5, 78, 98, 0];
 //filter even number only
var res = arr.some(function(value) { //some function given true false
  return value % 2 === 1; // odd number
});
console.log(res); //true

var res = arr.some(function(value) { //some function given true false
  return value < 0;
});
console.log(res); //false