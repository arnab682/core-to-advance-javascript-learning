
function sample( a, b, cb) { // this is main function and cb is callback function
  var c = a + b;
  var d = a-b;
  var result = cb(c, d);

  return result;
}//Anonymas function

function sum(c, d) {
  return c+d;
}
function sub(c, d) {
  return c-d;
}
function mul(c, d) {
  return c*d;
}
function dev(c, d) {
  return c/d;
}
function mod(c, d) {
  return c%d;
}

console.log(sample(5, 2, sum));
console.log(sample(5, 2, sub));
console.log(sample(5, 2, mul));
console.log(sample(5, 2, dev));
console.log(sample(5, 2, mod));

var result1 = sample(3, 4, function(c, d){
  return c==d;
});
console.log(result1);