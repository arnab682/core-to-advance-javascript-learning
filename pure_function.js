// '* It Returns the same result if given the same arguments'
// '* It does not cause any observable side effects'

function sqr(n) {
  return n*n; //it is pure function cz it is no effect in any function means no side affect
}
console.log(sqr(2));
console.log(sqr(2));
console.log(sqr(2));

var m = 10;
function change(){
 m = 7;//It have side effect
}
console.log(m); // m = 10

//but
change();
console.log(m); // m = 7 // not pure function cz it is change the value

var point = { //not puer function
  x: 45,
  y: 30
}

function printPoint(point) {
  point.x = 100;
  point.y = 200;

  console.log(point);
}
printPoint(point);
console.log(point);
