// Logic and Condition
// if, else, else if, switch  statement

var n = 47;

if(n%2 === 0){
  console.log(n + ' is this number even');
} 
else if( n%2 === 1){
  console.log(n + ' is this number odd');
}
else{
  console.log(n + ' is this number');
}


//switch statement
var date = new Date();

// Day find
var today = date.getDay();
console.log(today);

var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

switch(today){

  case 0:
    console.log('Today is ' + days[today]);
    break;
  case 1:
    console.log('Today is ' + days[today]);
    break;
  case 2:
    console.log('Today is ' + days[today]);
    break;
  case 3:
    console.log('Today is ' + days[today]);
    break;
  case 4:
    console.log('Today is ' + days[today]);
    break;
  case 5:
    console.log('Today is ' + days[today]);
    break;
  case 6:
    console.log('Today is ' + days[today]);
    break;
  default :
    console.log('This day not defind');
}


//Ternary Operator

var n = 10;
var str = '';

if (n%2 === 0){
    str = 'EVEN';
} else {
    str = 'ODD';
}
console.log(str);

// Ternary Operator example
var m = 11;
var strTernay = (m%3) === 0 ? 'EVEN' : 'ODD'; // condition ? true : false
console.log(strTernay);

//Shorthand
var name = '';
var fullName = name || 'Arnab';
console.log(fullName);

var name1 = 'Anik';
var fullName1 = name1 || 'Arnab';
console.log(fullName1);

//shorthand &&
var isOk = true;
isOk && console.log('Everthing is OK');

var isNo = false;
isNo && console.log('Everthing is OK');