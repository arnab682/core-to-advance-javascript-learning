//....foreach....
var arr = [1,2,3];
arr.forEach(function(value, index, array) {
  console.log(value);
});

//vs
//....map....
var sqrArray = arr.map(function(value) {
  return value * value;
});
console.log(arr);
console.log(sqrArray);

//....try to create map function....
function myMap(arr) {
  var newArray = [];
  for (var i = 0; i < arr.length; i++) {
    var temp = arr[i] * arr[i];
    newArray.push(temp);
  }
  return newArray;
}
console.log(myMap(arr));


var arr = [1,2,3,4];
//...using callback function...
function myMap2(arr, cb) {
  var newArray = [];
  for (var i = 0; i < arr.length; i++) {
    var temp = cb(arr[i], i, arr); // value, index, array
    newArray.push(temp);
  }
  return newArray;
}
// callback function for myMap
var sqr = myMap2(arr, function(value){
  return value * value;
});
var qb = myMap2(arr, function(value){
  return value * value * value;
});
var mul = myMap2(arr, function(value){
  return value * 10;
});
console.log(arr);
console.log(sqr);
console.log(qb);
console.log(mul);