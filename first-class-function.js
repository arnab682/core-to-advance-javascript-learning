function add(a, b) {
  return a+b;
}
// '* A function can be stored in a variable'
var sum = add;
console.log(sum(2, 5));
console.log(typeof sum);

// '* A function can be stored in an array'
var arr = [];
arr.push(add);
console.log(arr);
console.log(arr[0](5, 2));

// '* A function can be stored in an object'
var obj = {
  sum: add
}
console.log(obj);
console.log(obj.sum(7, 9));
// '* we can create function as need'
setTimeout(function(){
  console.log('Hi Arnab...');
}, 100);
// '* we can pass function as an Arguments'
// '* we can return function from Another function from Another Function'

