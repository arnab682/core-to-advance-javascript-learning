//function 

var date = new Date();
console.log(date.getFullYear());

function welcomeFunction() {
  console.log('I am function');
}
welcomeFunction(); //Function call

for (var i = 0; i <= 10; i++) {
  welcomeFunction();
}


//Arguments and parameter of a function 
function sum(x, y) {
  console.log(x + y);
}
sum(5, 2);//

// array sum 
var arr1 = [1, 2, 3];
var arr2 = [10, 20, 30];
var arr3 = [11, 12, 13];

function sum1(a) { //a is a parametter
  var sum1 = 0;
  for (var i = 0; i < a.length; i++) {
    //console.log(a[i]);
    sum1 += a[i];
    //console.log(sum1);
  }
  console.log(sum1);
}
sum1(arr1); //Argument
sum1(arr2);
sum1(arr3);



//Argument Object Function

function test (a,b,c) {
  console.log(arguments); //object -> arguments
  console.log(JSON.stringify(arguments)); //object -> arguments
  console.log(typeof arguments);

}
test();

//.....
function test () {
  //console.log(arguments); //object -> arguments
  //console.log(JSON.stringify(arguments)); //object -> arguments
  //console.log(typeof arguments);
  for (var i = 0; i < arguments.length; i++) {
    console.log('index ' + i + ': ' + arguments[i]);
  }

}
test(10, 20, 30);


function sumArgument() {
  var sum = 0;
  for (var i = 0; i < arguments.length; i++) {
    sum += arguments[i];
  }
  console.log(sum);
}
sumArgument(1, 2, 3);
sumArgument(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

var a = sumArgument(1, 2, 3);
var b = sumArgument(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
console.log(a, b); // It's undefind because this function do not return his result


// return 
function call(){
  return 'Hello';
}
var c = call(); // I do store this function result because i return this function 
console.log(c);
console.log(typeof c);


function person(name, email) {
  return {
    name: name,
    email: email
  }
}
var p1 = person('Arnab', 'arnab@gmail.com');
console.log(p1);
console.log(typeof p1); // object
console.log(JSON.stringify(p1));
console.log(typeof JSON.stringify(p1)); //string


function call1(){
  return 'Hi';
  console.log('Hi'); // this is not runing after return call
}
console.log(call1());

function call2(){
  console.log('Hai'); // this is runing before return call
  return 'Hai';
}
//call2();
console.log(call2());


//Function Expression
var addition = function(a, b){ //Add is a anonymous function
  return a+b;
} 
console.log(addition(10, 20));


//Biulding method
setTimeout(function () {
  console.log('I will call after 5 second');
}, 5000)

var another = addition;
console.log(another(7, 8)); //It is console then afte 5 second call setTimeout this system call is asynchronous


//Inner function
function something(greet, name) {
  function sayHi() {
    console.log(greet, name);
  }

  sayHi();
}
something('Good Morning', 'Arnab Das');


//filter first name
function something(greet, name) {
  function getFirstName() { //Child function
    if (name){
      return name.split(' ')[0];
    } else {
        return '';
    }
  }

  var message = greet + ' ' + getFirstName();
  console.log(message);
}
something('Hello Bro!', 'Arnab Das Anik');


//function scoping

var a = 'abc';
// if (true) {
//   if (true) {
//     var a = 10;
//     console.log(a); // a value change a = 10
//   }
//   console.log(a); // a value change a = 10
// }
// console.log(a); // a value change a = 10

function x () {
  function y () {
    var a = 20;
    console.log(a); // a value change a = 20
  }
  console.log(a); // a value not change a = abc
  y();
}
x();
console.log(a); // a value not change a = abc

//or
function x () {
  var a = 40;
  function y () {
    console.log(a); // a value change a = 40
  }
  console.log(a); // a value change a = 40
  y();
}
x();
console.log(a); // a value not change a = abc


//child function using parent function's all data
function test(n) {

  function a() {
    return n%3 === 0;
  }

  function b() {
    return n%5 === 0;
  }

  if(a() && b()) {
    console.log(n + ' is divisible by boot 3 and 5.');
  } else {
      console.log(n + ' is not divisible.');
  }
}
test(15);