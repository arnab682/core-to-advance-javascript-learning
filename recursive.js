//Recursive function
function sayHi(n){
  if(n === 0){
    return true;
  }
  console.log('Hi Arnab...' + n);
  sayHi(n-1);
}
sayHi(10);

//sum
function sum(n) {
  if(n === 1) {
    return 1;
  }
  return n + sum(n-1); //n = 5+4  9+3 12+2 14+1
}
console.log(sum(5));


//multiplu
function fact(n) {
  if(n === 1) {
    return 1;
  }
  return n * fact(n-1); //5*4 20*3 60*2 120*1
}
console.log(fact(5));


//Array Sum
var arr1 = [1, 2, 3, 4, 5];
function sumOfArray(arr, lastIndex) {
  if(lastIndex < 0) {
    return 0;
  }
  return arr[lastIndex] + sumOfArray(arr, lastIndex -1);
}
var value = sumOfArray(arr1, arr1.length-1);
console.log(value);