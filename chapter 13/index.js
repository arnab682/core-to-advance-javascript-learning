var obj = {}
console.log(obj); //__proto__

var obj2 = new Object();
console.log(obj2); //__proto__

var arr = [];
console.log(arr); //__proto__  prototype means parent

var str = new String('str');
console.log(str); //__proto__  prototype means parent


function Person(name) { //constructor
  this.name = name;
  console.log(this);
}

var p1 = new Person('Arnab Das'); //function store in class and create a object
console.log(p1); //Multiple __proto__


var person = {
  name: 'Arnab'
}
console.log(person);

for (var i in person){
  console.log(i);
}
console.log(Object.keys(person));


var descriptor = Object.getOwnPropertyDescriptor(person, 'name');
console.log(descriptor);
//congigurable: true means removeable not for outsite
//enumerable: true means not find loop from outsite
//value: "Arnab"
//writable: true means it is changeable from outsite

var baseObj = Object.getPrototypeOf(person);
var descriptor = Object.getOwnPropertyDescriptor(person, 'name');
console.log(descriptor);


Object.defineProperty(person, 'name', {
  enumerable: false,
  writable: false,
  configurable: false,
  //value: 'Anik'
})
//for check in browser
//person
//Object.keys(person) //enumerable: false,
//for (var i in person){ //enumerable: false,
//  console.log(i)
//}
//person.name = 'Anik' //writable: false,
//person   //writable: false,
//delete person.name  //configurable: false
//person   //configurable: false


function Person1(name) {
  this.name = name;
}

Person1.prototype.PI = 3.1416; //create a property in Person class

var p1 = new Person1('Anik');
var p2 = new Person1('Arnab');
// console.log(Object.getPrototypeOf(p2)); //prototype
// console.log(Person1.getPrototypeOf);

console.log(p1);
console.log(p2);


//add property in object
let a = {}
console.log(a);
Object.prototype.PI = 3.1416;
console.log(a.__proto__);


var arr = [];
console.log(arr);
Array.prototype.myMethod = function() {};
console.log(arr.__proto__);



//Instance vs Prototype Members
function Square (width) {
  this.width = width; //instant member

  // this.draw = function() {
  //   console.log('Draw');
  // }

  this.getWidth = function() {
    console.log('Width is ' + this.width);
    this.draw();
  }
}
//prototype use for draw method
//prototype use for ignor dublicate when many obj for one class
Square.prototype = { //prototype member
  draw: function() {
       console.log('Draw');
   },
  toString: function () {
    this.getWidth();
    return 'My width is = ' + this.width;
  }
}
var sqr1 = new Square(10);
var sqr2 = new Square(5);

console.log(sqr1);
console.log(sqr1.draw());
console.log(sqr1.toString());

console.log(sqr2);
console.log(sqr2.draw());
console.log(sqr2.toString());

//now prototype member function using in class instant member
console.log(sqr1);
console.log(sqr1.getWidth());

//now class instant member using in prototype member function
console.log(sqr1);
console.log(sqr1.toString());


//another class
//Iterate Object Properties
console.log(sqr1.hasOwnProperty('width')); //true
console.log(sqr1.hasOwnProperty('getWidth')); //true
console.log(sqr1.hasOwnProperty('draw')); //false

//check it property
console.log(Object.keys(sqr1));
for (var i in sqr1) {
  console.log(i);
}


//Another class
//don't overwrite built in prototypes in JS Object
//Array.prototype.shuffle = function() {}
//[].shuffle //it is a great problem
//shuffle auto show after dot
