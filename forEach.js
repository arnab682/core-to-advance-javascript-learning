var arr = [1, 2, 3, 4];

arr.forEach(function(value, index, arr){
  console.log(value, index, arr);
});

var sum = 0;
arr.forEach(function(value, index, arr){
  //console.log(value, index, arr);
  sum += value;
});
console.log(sum);

//....for
function foreach(arrAgument) {
  for(var i = 0; i < arrAgument.length; i++) {
    console.log(arr[i], i, arrAgument);
  }
}
foreach(arr);

function foreach2(arrAgument, cb) { //callback function
  for(var i = 0; i < arrAgument.length; i++) {
    cb(arrAgument[i], i, arrAgument);
  }
}
var sum1 = 0;
foreach2(arr, function(value, index, arrAgument){ 
  console.log(value, index, arrAgument);
  sum1 += value;
});
console.log(sum1);


foreach(arr, function(value, index, arr){
  arr[index] = value + 5;
  
});
console.log(arr);