// var rect = {
//   width: 100, //property
//   height: 50,

//   draw: function() { //method
//     console.log('I am a rectangle');
//     console.log('My width is ' + this.width);
//     console.log('My height is ' + this.height);
//   }
// }
// rect.draw();
// rect.height = 100;
// rect.draw();

// //try to know (this)
// function myFuc(){
//   console.log(this); //this refer window object cz myFuc not in a object
// }
// //myFuc(); //window this
// new myFuc();

//another class
// var rect = {
//   width: 100, //property
//   height: 50,

//   draw: function() { //method
//     console.log('I am a rectangle');
//     this.printProperties();
//     console.log(this);
//   },

//   printProperties: function() {
//     console.log('My width is ' + this.width);
//     console.log('My height is ' + this.height);
//   }
// }
// rect.draw();


// //now i am using ract.printProperties in another object
// var another = {
//   width: 77,
//   height: 88,
//   print: ract.printProperties
// }
// console.log(another.print);
// another.print();



//another class
//Create class using factory pattern
// var createRect = function(width, height) {
//   return {
//     width: width, //property
//     height: height,

//     draw: function() { //method
//       console.log('I am a rectangle');
//       this.printProperties();
//       console.log(this);
//     },

//     printProperties: function() {
//       console.log('My width is ' + this.width);
//       console.log('My height is ' + this.height);
//     }
//   }
// }
// var rect1 = createRect(22, 44);
// rect1.draw();

// var rect2 = createRect(26, 84);
// rect2.draw();


//another class
//construct patten
var Rectangle = function(width, height) { 
  this.width = width; //property
  this.height= height;

  this.draw = function() { //method
    console.log('I am a rectangle');
    this.printProperties();
    console.log(this);
  }

  this.printProperties = function() {
    console.log('My width is ' + this.width);
    console.log('My height is ' + this.height);
  }
}
var rect3 = new Rectangle( 12, 42); //using new for create empty object
rect3.draw();


//another class  
//create same empty object new manualy
function myNew(constructor) {
  var obj = {} //empty object  it's like  var obj = new Object()
  Object.setPrototypeOf(obj, constructor.prototype);
  var argsArray = Array.prototype.slice.apply(arguments);
  constructor.apply(obj, argsArray.slice(1));

  return obj;
}
var rect4 = myNew(Rectangle, 32, 85);
rect4.draw();


var str = new String('str');
console.log(str.constructor);

//function is a spacial typeof object
//only primitive datatype is a object in js
//prove that 1
function test(){
  console.log('Something');
}
test();
console.log(test.constructor); //every object have a constructor

//prove that 2
var Rect = new Function('width', 'height', 
  `this.width = width, //property  string value
  this.height= height,

  this.draw = function() { //method
    console.log('I am a rectangle');
    this.printProperties();
    console.log(this);
  }

  this.printProperties = function() {
    console.log('My width is ' + this.width);
    console.log('My height is ' + this.height);
  }` );

var rect5 = new Rect(2,5);
console.log(rect5);

//prove that 3
function test(){
  console.log('Something');
}
console.log(test.name, test.length);


//apply call
//bind

function myFunc(){
  console.log(this);
  console.log(this.a + this.b);
}
myFunc.call({});
myFunc.apply({});
myFunc.bind({});
//myFunc();
console.log('call, apply & bind function');
myFunc.call({a: 40, b: 20});
myFunc.apply({a: 40, b: 20});
var bindVariable = myFunc.bind({a: 40, b: 20});
bindVariable();

function myFunc1(c,d){
  console.log(this);
  console.log(this.a + this.b + c + d);
}
myFunc1.call({a: 40, b: 20}, 10, 3);
myFunc1.apply({a: 40, b: 20}, [10, 3]); // array provied for apply function
var bindVariable1 = myFunc1.bind({a: 40, b: 20}, 10, 3);
bindVariable1();
var bindVariable2 = myFunc1.bind({a: 40, b: 20});
bindVariable2(10,3);


//Pass by value vs pass by reference
//call by value vs call by reference
 var n = 10;

 function change(n){
   n = n + 100;
   console.log(n);
 }
 change(n);
 console.log(n);

 var obj = {
   a: 10,
   b: 20
 }
 function changeMe(obj) {
   obj.a = obj.a + 100;
   obj.b = obj.b + 100;
   console.log(obj);
 }

 changeMe(obj);
 console.log(obj); //object data change


// //Abstruction
// var Rectangle = function(width, height) { 
//   this.width = width; //property
//   this.height= height;
//   var position = { //local variable var or let using for abstruction
//     x: 7,
//     y: 5
//   };
//   console.log(position);
  
//   let str = 'Abstruction';
  
//   console.log(str);

//   var printProperties = function() { //local variable var or let using for abstruction
//     console.log('My width is ' + this.width);
//     console.log('My height is ' + this.height);
//   }.bind(this);

//   this.draw = function() { //method
//     console.log('I am a rectangle');
//     printProperties();
//     console.log('Position: X=' + position.x + ' Y=' + position.y);
//     console.log(this);
//   }
// }
// var rect7 = new Rectangle(2, 5);
// console.log(rect7.position);
// rect7.draw();



//If i needed to use Abstruction property
//solve
var Rectangle = function(width, height) { 
  this.width = width; //property
  this.height= height;
  var position = { //local variable var or let using for abstruction
    x: 7,
    y: 5
  };
  console.log(position);

  //one solution for use abstruction property
  this.getProperty = function () {
    return position;
  }
  //......

  //second solution for use abstruction property
  Object.defineProperty(this, 'positonData', {
    get: function() { //for data get
      return position;
    },

    set: function(value) {
      position = value;
    }
  })
  
  let str = 'Abstruction';
  console.log(str);

  var printProperties = function() { //local variable var or let using for abstruction
    console.log('My width is ' + this.width);
    console.log('My height is ' + this.height);
  }.bind(this);

  this.draw = function() { //method
    console.log('I am a rectangle');
    printProperties();
    console.log('Position: X=' + position.x + ' Y=' + position.y);
    console.log(this);
  }
}
var rect7 = new Rectangle(2, 5);
console.log(rect7.position);
rect7.draw();
console.log(rect7.getProperty());//one solution for using abstruction property

//second solution for using abstruction property
console.log(rect7.positonData);
rect7.positonData = {
  x: 465,
  y: -123
}
console.log(rect7.positonData);
