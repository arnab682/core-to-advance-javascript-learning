var int = 22;
var doub = 24.23;
var str = 'Hello2';

console.log(Number.parseFloat(int));
console.log(Number.parseInt(doub));
console.log(Number.parseFloat(str));
console.log(Number.MAX_SAFE_INTEGER);
console.log(Number.MAX_VALUE);
console.log(Number.MIN_SAFE_INTEGER);
console.log(Number.MIN_VALUE);

console.log(1/0);
console.log('abc' * 1);



//.......String...........
//string literal
var str1 = 'Hello1';
var str2 = "Hello2";
var str3 = `Hello3`;

console.log(str1, str2, str3);



var str4 = String('Hello4');
var str5 = String(156);
var str6 = String(3.1416);

console.log(str4, str5, str6);

var bool1 = true;
var bool2 = false;
var bool3 = Boolean(1);
var bool4 = Boolean(0);

console.log(bool1, bool2, bool3, bool4);


var abc1;
var abc2 = null;

console.log(abc1, abc2) //undefined null


//.....Type Conversion......

var str = '1000'; //String
var str1 = 'Num1000'; //String
var n = 10 //number

console.log(Number(str));//String to Number Convert
console.log(Number.parseInt(str));

console.log(Number(str1));

console.log(typeof str)
console.log(typeof n)

console.log(n.toString());
console.log(typeof n.toString());

console.log(Number(Infinity));//always infininity
console.log(String(Infinity)); //converted String
console.log(typeof String(Infinity));

console.log(Boolean(Infinity));
console.log(Boolean(-Infinity));

console.log(true);
var tr = true;
console.log(tr.toString());


//....Constructor Function....
//Number Boolean String

//...........Falsy Values List.........
//  ''  0  null  undefined false NaN
console.log(Boolean(''));
console.log(Boolean(0));
console.log(Boolean(null));
console.log(Boolean(undefined));
console.log(Boolean(false));
console.log(Boolean(NaN));


//....Hexadecimal & Octal Number

var hex = 0xff;//Hexa Number f=15
console.log(hex);

var oct = 0756;
console.log(oct);


//....Arithmetic Operator....
// + - * / % ++ --

console.log(2+5); // + arithmetic operator
console.log(10-3); // - arithmetic operator
console.log(7*1); // * arithmatic operator
console.log(7/1); // / arithmatic operator
console.log(7%2); // % arithmatic operator

var a = 7;
console.log(a); //7
console.log(a++); //7
console.log(a); // 8

var b = 7;
console.log(b); //7
console.log(++b); //8
console.log(b); // 8


var c = 7;
console.log(c); //7
console.log(c--); //7
console.log(c); //6

var d = 7;
console.log(d); //7
console.log(--d); //6
console.log(d); //6


//Assignment Operator
// += -= /= *= %=

var a1 = 5;
var b1 = 2;
console.log(a1+=b1);
console.log(a1-=b1);
console.log(a1/=b1);
console.log(a1*=b1);
console.log(a1%=b1);


//Comparison Operator
// == > <

var value1 = 20;
var value2 = 10;

console.log(value1 == value2);
console.log(value1 > value2);
console.log(value1 < value2);
console.log(value1 >= value2);
console.log(value1 <= value2);

var str = '50';
var num = 50;

console.log(str == num);
console.log(str === num);


//Logical Operator
//     &&  ||  !
var x = 1;
var y = 2;
console.log(y>x && x==y )
console.log(y<x || x !=y )
console.log(x !=y )


//Bitwise Operator for binary number
// & | ~ ^ << >>

// var g = 5;
// var h = 9;
// console.log(g&h);
// console.log(g|h);
// console.log(~g);
// console.log(g<<h);
// console.log(g>>h);



//Statement
console.log('Hello World'); //Simple statement
var str = 'String';//statement
var number = 10 + (3/5) * 5;//statement
var a = 10;//statement
var b = 20;//statement
var c = a>=b;//statement
console.log(c);//statement

//complex statement if switch


//Math Function Provided
console.log(Math.E);
console.log(Math.PI);

var n = 4.589;
console.log(Math.abs(n));
console.log(Math.floor(n));
console.log(Math.ceil(n));
console.log(Math.round(n));
console.log(Math.max(400,500));
console.log(Math.min(100,40));
console.log(Math.log(10));
console.log(Math.pow(2, 3));
console.log(Math.sqrt(81));
console.log(Math.round(Math.random()*50 + 1));


//date function
var date = new Date();
console.log(date);
console.log(date.toDateString());
console.log(date.toTimeString());
console.log(date.toLocaleString());
console.log(date.toLocaleDateString());
console.log(date.getFullYear());
console.log(date.getMonth());
console.log(date.getTime());
console.log(date.getTimezoneOffset());
console.log(date.getHours());
console.log(date.getSeconds());
console.log(date.getUTCFullYear());

