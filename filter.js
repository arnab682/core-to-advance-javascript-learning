var arr = [1, 2, 4, 5, 8, 0];

var filterArray = arr.filter(function(value) {
  return value%2 == 0; //even
});
console.log(filterArray);

var filterArray = arr.filter(function(value) {
  return value%2 == 1; //odd
});
console.log(filterArray);


//....
function myfilter(arr, cb) {
  var newArray = [];
  for (var i = 0; i < arr.length; i++){
    if (cb(arr[i], i, arr)){
      newArray.push(arr[i]);
    }
  }
  return newArray;
}
var even = myfilter(arr, function(value){
    return value%2 == 0;
});
var odd = myfilter(arr, function(value){
  return value%2 == 1;
});
console.log(even);
console.log(odd);
