// First Look at Closure

//
var b = 10;
function a() {
  console.log(b); //It is not closure cz b is a global variable it is not argument
}

//...........
function c() {
  var x = 5; 
  return function() {
    console.log(x); // x is a closure cz it is only global for this function
  }
}
var abc = c();
console.dir(abc); // dir use for details {this code run a bowser}