var isRunning = true;

while(isRunning) {
  var rand = Math.floor(Math.random() * 10 + 1)
  if(rand === 9){
    console.log('Winner winner Chicken Dinner');
    isRunning = false; //while loops stop
  } else {
      console.log('You have got ' + rand);
  }
}

console.log('do while');


// do while
var isRunning1 = true;
do{
  var rand = Math.floor(Math.random() * 10 + 1)
  if(rand === 9){
    console.log('Winner winner Chicken Dinner');
    isRunning1 = false; //while loops stop
  } else {
      console.log('You have got ' + rand);
  }
}while(isRunning1)


//NEsted Loop
//1
//1 2
//1 2 3
//1 2 3 4
//1 2 3 4 5
var n = 5;
for (var i = 1; i <= n; i++){
  console.log('i value ' + i);
  for (var j = 1; j <= i; j++){
    console.log('j value ' + j);
  }
}

console.log('OR');

var m = 5;
for (var i = 1; i <= m; i++){
  var results = '';
  //console.log('i value ' + i);
  for (var j = 1; j <= i; j++){
    results += j + ' ';
  }
  console.log('j values ' + results);
}

// *****
// *****
// *****
// *****
// *****
console.log('Star =>');
var star = 5;
for (var i = 1; i <= star; i++){
  var results = '';
  for(var j = 1; j <= star; j++){
    results += '*';
  }
  console.log(results);
}



// break continue
console.log('Break Example:')
while(true) {
  var rand = Math.floor(Math.random() * 10 + 1)
  if(rand === 9){
    console.log('Winner winner Chicken Dinner');
    break; //while loops stop
  } else {
      console.log('You have got ' + rand);
  }
}

console.log('Continue Example:')
while(true) {
  var rand = Math.floor(Math.random() * 10 + 1)
  if(rand === 9){
    console.log('Winner winner Chicken Dinner');
    continue; //while loops continue
  } else {
      console.log('You have got ' + rand);
      break;
  }
}

//Continue Statement
for ( var i =0; i < 10; i++){
  if(i === 3 || i ==7){
    continue;
  } else {
      console.log(i);
  }
}

//Infinity loop
for(; ;) {
  var rand = Math.floor(Math.random() * 10 + 1)
  if(rand === 9){
    console.log('Winner winner Chicken Dinner');
    break;
  } else {
      console.log('You have got ' + rand);
  }
}