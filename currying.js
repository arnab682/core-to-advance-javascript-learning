function add(a, b, c) {
  return a + b + c;
}
console.log(1+4+2);

//currying
function currying(a) {
  return function(b) {
    return function(c) {
      return a + b + c;
    }
  }
}
console.log(currying(1)(4)(2));