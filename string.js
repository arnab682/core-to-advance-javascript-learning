var num = 3;
var str = 'this is num'; //string literal
var str1 = "this is num";
var str2 = "this is "+num;
var str3 = String('"this is num"'); //String Constructor
var str4 = String('"this is num"');
var str5 = String('"this is- "'+num);
var str6 = String('this is- '); // '' or "" same means for JS 


console.log(str);
console.log(str1);
console.log(str2);
console.log(typeof str2);
console.log(str3);
console.log(str4);
console.log(str5);
console.log(str6);


//String Literal vs String Constructor

var num = 5;
var str = '20';
console.log(str+num);
console.log(num+str);

var num = 5;
var str = String('20');
console.log(str+num);
console.log(num+str);


var n = 5;
var str = n;
var str1 = n + ''; // + '' (String) means converted string
var str2 = n.toString();
var str3 = String(n);
console.log(str); //Number
console.log(str1); //String
console.log(str2); //String
console.log(str3); //String


//Escape Notation

var str = 'This is a \'String\''; //   \ Escape character
var str1 = 'This is a \nString';   //   \n Escape character
var str2 = 'This is a \tString';   //   \t Escape character
var str3 = 'This is a \\String';   //   \ Escape character
var str4 = 'This is a \rString';   //   \ Escape character
var str5 = 'This is a \vString';   //   \ Escape character
var str6 = 'This is a \bString';   //   \ Escape character
var str7 = 'This is a \fString';   //   \ Escape character

//   \r Carriage Return   \v Vertical Tab  \b Backspace   \f Form Feed

console.log(str);
console.log(str1);
console.log(str2);
console.log(str3);
console.log(str4);
console.log(str5);
console.log(str6);
console.log(str7);


//Compare two string
var a = 'abc';
var b = 'cba';

console.log(a === b); //Lexical Order z,y,x,...a, Z,Y,X,....,B,A
console.log(a>b); //false
console.log(a<b); //true

console.log('z' > 'Z') //every smail letter bigger from any capital letter

console.log('001' == 1); //true cz String converted to number so 001 to 1
console.log('001' === 1); //false

//String Method
var str = 'I am';
var str1 = 'Arnab Das';
var c = str.concat(' ', str1);
//var d = c.substr(10);
var d = c.substr(5,5);

console.log(c);
console.log(d);
console.log(c.charAt(6));
console.log(c.startsWith('r'));
console.log(c.endsWith('s'));
console.log(c.toUpperCase());
console.log(c.toLowerCase());
console.log('  Arnab Das  '.trim());
console.log(c.split(' '));



//Length of string 
var str = 'Some String';
console.log(str.charAt(7)); //r
console.log(str.charAt(12)); // empty string
var store = str.charAt(12);
console.log(store);
console.log(typeof store);

//Find out String Length
var str = 'Some String';
var length = 0;

while (true){
  if(str.charAt(length) == ''){
    break;
  } else {
      length++;
  }
}
console.log(length);

console.log(str.length);
console.log(' Anik  '.length);

